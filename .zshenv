typeset -U PATH path
export PATH=$PATH:$HOME/.scripts/
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:/opt/riscv/bin
export PATH=$PATH:/home/bartold/dl/verifast-21.04/bin
export PATH=$PATH:/opt/urserver
export TEXMFHOME=$HOME/texmf

# Python
# Stage
export PYTHONPATH=$PYTHONPATH:/home/bartold/dox/werk/cosic_stage/ArxPy


# zsh prompt themes
export FPATH=$FPATH:$HOME/.zprompts/

export TERM=st-256color

export PLANTUML_LIMIT_SIZE=8192
export ANDROID_SDK_ROOT=/home/bartold/Android/Sdk
export ANDROID_HOME=/home/bartold/Android/Sdk

