" vim:foldmethod=marker:foldlevel=0
" Plugins {{{

call plug#begin()

" Tagbar
Plug 'majutsushi/tagbar'

" Sagemath
Plug 'petRUShka/vim-sage'

" Org mode
Plug 'jceb/vim-orgmode'
Plug 'tpope/vim-speeddating'
Plug 'vim-scripts/utl.vim'

" Easier commenting
Plug 'preservim/nerdcommenter'

" Fuzzy finding
Plug 'junegunn/fzf.vim'

" LaTeX
Plug 'lervag/vimtex'

" Python
" Better folding
Plug 'tmhedberg/SimpylFold'
" Better indentation
Plug 'Vimjas/vim-python-pep8-indent'

" Snippetssss
Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'

" Rust
Plug 'rust-lang/rust.vim'

" YouCompleteMe
Plug 'ycm-core/YouCompleteMe'

" Faster folding
Plug 'Konfekt/FastFold'

" Emmet (HTML)
Plug 'mattn/emmet-vim'

" Case-memorizing replace
Plug 'tpope/vim-abolish'

" Arduino
"Plug 'sudar/vim-arduino-syntax'
"Plug 'sudar/vim-arduino-snippets'
Plug 'stevearc/vim-arduino'

" Better pane/window switching with tmux
Plug 'christoomey/vim-tmux-navigator'

" Terminal commands
Plug 'benmills/vimux'

" PlantUML
Plug 'tyru/open-browser.vim'
Plug 'aklt/plantuml-syntax'
Plug 'weirongxu/plantuml-previewer.vim'

" ALE
Plug 'dense-analysis/ale'

" VimWiki
Plug 'vimwiki/vimwiki'

" Agda
Plug 'coot/vim-agda-integration'

call plug#end()

" }}}

" Change mapleader from \ to ,
let mapleader=","
" Local mapleader still \
let localmapleader="\\"


" General stuff {{{
" Math in Vim! Press C-A after expression, it calculates!
inoremap <C-A> <C-O>yiW<End>=<C-R>=<C-R>0<CR>
" Set shell
set shell=/bin/zsh
" lazyredraw: no redraw while macro and stuff
set lazyredraw
" Relative line numbers
set number
"set relativenumber
" Auto syntax coloring
filetype indent on
syntax on
" Tab stuff
set expandtab " Expands tabs to spaces <3
autocmd FileType * set tabstop=4
autocmd FileType * set softtabstop=4
autocmd FileType * set shiftwidth=4
" Always show sign column
set signcolumn=yes
" Split horizontal windows below (instead of above)
set splitbelow
" Copy indent from line above
set autoindent
" Smarter indentation (cindent is also possible)
set smartindent
" Search
set ignorecase " Case insensitive
set smartcase " Case sensitive if upper case used in search

set hlsearch " Highlight search terms
set incsearch " Searches when typing
nnoremap <silent> <leader><Space> :nohlsearch<Enter>:call clearmatches()<Enter>" Remove highlight when ,space typed

nnoremap n nzzzv" Search term in middle of screen
nnoremap N Nzzzv

" Makes switching between corresponding braces easier
nnoremap <Tab> za

" Go down/up one VIRTUAL line
nnoremap j gj
nnoremap k gk

nnoremap 0 ^

" 5 lines above/below cursor visible
set scrolloff=5

" Change buffers when not saved
set hidden

" Folding
set foldmethod=syntax
set foldlevel=0

" Fix not deleting characters (visually) in eg. git commit messages
set backspace=indent,eol,start

" Close all buffers but current one
" Close all buffers (%bd); then edit last one (e#); then close last one (No
" Name buffer)
command BufOnly :%bd|e#|bd#
nnoremap <leader>b <Plug>Kwbd

nnoremap <leader>s :set spell<CR>:set spelllang=en,nl<CR>

" }}}

" Filetype specific {{{
autocmd BufEnter *.idp set filetype=idp

" LaTeX
let g:tex_flavor = 'latex'

""" Python
" Flag unnecessary whitespace
"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

"python with virtualenv support
"py << EOF
"import os
"import sys
"if 'VIRTUAL_ENV' in os.environ:
"project_base_dir = os.environ['VIRTUAL_ENV']
"activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"execfile(activate_this, dict(__file__=activate_this))
"EOF
" }}}

" Misc Configuration {{{
:autocmd BufWritePost ~/.Xresources !xrdb ~/.Xresources
" }}}

" Statusline {{{
set laststatus=2

set statusline=
set statusline+=%#keyword#\ %f
set statusline+=\ %y
set statusline+=%m
set statusline+=%=
set statusline+=%#function#\ %P
set statusline+=\ %l
set statusline+=/%L
set statusline+=\ 
" }}}

" Enable project specific vimrc {{{
" Enable vim to parse local .vimrc
set exrc
" Prevent autocmd, shell, ... commands to be run if they're not mine
set secure
" }}}

" Plugin config {{{
" fzf
nnoremap <C-p> :Files<Enter>

" vimtex
let g:vimtex_view_method = 'zathura'
let g:vimtex_fold_enabled = 1

" UltiSnips
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" YCM
let g:ycm_autoclose_preview_window_after_completion=1
nnoremap <silent> <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>zO

" Vimtex no underfull, overfull hbox warnings in quickfix
"let g:vimtex_log_ignore = "*[Underfull|Overfull]*"
"
" ALE
nnoremap <leader>n :ALENext<CR>
" Only lint when file save
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:ale_lint_on_enter = 1
let g:ale_lint_on_save = 1
" No background
"highlight clear ALEErrorSign
"highlight clear ALEWarningSign
"let g:ale_set_highlights = 1

" Vimux
nnoremap <Leader>vp :VimuxPromptCommand<CR>
nnoremap <Leader>vl :VimuxRunLastCommand<CR>

" VimWiki
let g:vimwiki_list = [
            \{'path': '~/dox/wiki/', 'syntax': 'default', 'ext': '.wiki', 'template_path': '~/dox/wiki/templates', 'template_default': 'default', 'template_ext': '.html'},
            \{'path': '~/dox/wiki/academia', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/academia/crypto', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/academia/complexity', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/academia/formal', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/dnd', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/dnd/icespire_peak_uther', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/dnd/eberron', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/harmonica', 'syntax': 'default', 'ext': '.wiki'},
            \{'path': '~/dox/wiki/thm', 'syntax': 'default', 'ext': '.wiki'},
            \]
" }}}

" Theming {{{
highlight Folded ctermbg=14 ctermfg=16 cterm=bold
" Delete dashes after fold marker
set fillchars=fold:\ ,vert:\ ,eob:\ 
" Hide tildes after end of file
highlight EndOfBuffer ctermfg=0
" Background of search
highlight Search ctermbg=8
" Background of signcolumn
highlight SignColumn cterm=NONE
highlight clear SignColumn
" Make matching parentheses visible
highlight MatchParen ctermbg=8
" Vertical split line invisible
highlight VertSplit cterm=NONE
highlight VertSplit ctermfg=0
highlight ColorColumn ctermbg=8
highlight PMenu ctermbg=8
highlight LineNr ctermfg=4
highlight clear SpellBad
highlight SpellBad ctermfg=1 cterm=underline
" ALE
highlight ALEError cterm=NONE
highlight clear ALEError
highlight ALEError ctermfg=1 cterm=underline,bold
highlight ALEErrorSign ctermfg=1 cterm=bold
"highlight ALEErrorLine ctermbg=0 ctermfg=8
"highlight ALEErrorSignLineNr cterm=underline

"highlight ALEInfo ctermbg=0 ctermfg=8
"highlight ALEInfoSign ctermbg=0 ctermfg=8
"highlight ALEInfoLine ctermbg=0 ctermfg=8
"highlight ALEInfoSignLineNr ctermbg=0 ctermfg=8

highlight ALEWarning cterm=NONE
highlight clear ALEWarning
highlight ALEWarning ctermfg=3 cterm=underline,bold
highlight ALEWarningSign ctermfg=3 cterm=bold
highlight clear ALEWarningLine
"highlight ALEWarningLine ctermbg=0 ctermfg=8
"highlight ALEWarningSignLineNr ctermbg=0 ctermfg=8

"highlight ALEStyleError ctermbg=0 ctermfg=8
"highlight ALEStyleErrorSign ctermbg=0 ctermfg=8
"highlight ALEStyleErrorSignLineNr ctermbg=0 ctermfg=8

"highlight ALEStyleWarning ctermbg=0 ctermfg=8
"highlight ALEStyleWarningSign ctermbg=0 ctermfg=8
"highlight ALEStyleWarningSignLineNr ctermbg=0 ctermfg=8

" Know which groups are used for a certain thingy
nmap <leader>sp :call <SID>SynStack()<CR>
function! <SID>SynStack()
    if !exists("*synstack")
        return
    endif
    echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc
" }}}

" Tmux compatability (colors) {{{
set background=dark
set t_Co=256
" }}}

