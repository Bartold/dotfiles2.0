# Bartold's dotfiles 2.0
## Introduction
This will house all my dotfiles, along with documentation for the programs I use most often and scripts I wrote.

## Programs
### GPG
### Zsh
### Vim
### SSH
### Xorg
### i3
### xdg-user-dirs

## Scripts
All scripts can be found in the `.scripts` folder

**`buildwifi`** This rebuilds my wifi driver. The out-of the box driver doesn't use the right antenna. In `/etc/modprobe.d/rtl8723be.conf`, the option `options rtl8723be ant_sel=2` has to be set as well to make it work.

**`kb`** This sets the xkeyboard layout to the one I use (be, with numbers and symbols swapped on top row and caps and escape swapped as well)

**`passmenu`** Dmenu script to easily copy a password from pass.

**`buildst`** Builds and installs my fork of st

## Other
This is mostly to document configuration that's systemwide

**libinput** To configure touchpad/other input, make a config file at /etc/X11/xorg-conf.d/ (eg. 30-touchpad.conf) and add your configuration. More can be found in the man pages xorg.conf(5) (INPUTCLASS section) and libinput(4) (CONFIGURATION DETAILS).

